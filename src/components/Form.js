import React, {useState} from 'react'
import { useDispatch } from 'react-redux'
import { add } from '../store/actions/lists'


const Form = () => {
	const [text, setText] = useState("")
	const dispacth = useDispatch()
	const handleChange = e => {
		setText(e.target.value)
	}

	const sumbit = (e) => {
		e.preventDefault()
		dispacth(add(text))
		setText('')
	}

	return(
		<form onSubmit={sumbit}>
			<input type="text" value={text} onChange={handleChange} placeHolder="enter new task"/>
			<button>add</button>
		</form>
	)
}

export default Form
