import React from 'react'
import { Provider } from 'react-redux'
import logo from './logo.svg';
import Todos from './components/Todos';
import Form from './components/Form';
import './App.css';
import store from './store'

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Redux</h1>
          <Todos />
          <Form />
        </header>
      </div>
    </Provider>
  );
}

export default App;
